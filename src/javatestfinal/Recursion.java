package javatestfinal;

public class Recursion {
	public static void main(String[] args) {
		
		int [] x= {0, 30, 29, 10};
		System.out.println(recursiveCount(x, 0));
		
	}
	public static int recursiveCount(int[] x,int i) {
		if (i==x.length) {
			return 0;
		}
		else{
			if(x[i]>=20) {
				return 1+recursiveCount(x,i+1);
			}
			else if(x[i]%2!=0 && x[i]>=i) {
				return 1+recursiveCount(x,i+1);
			}
			return recursiveCount(x,i+1);
		}
	}

}
